# Installation for neovim

```
## clone project
cd ~/git/
git clone https://github.com/jaak-s/vimrc

## create symlink
mkdir -p ~/.config/nvim/
ln --symbolic ~/git/vimrc/init.vim ~/.config/nvim/

## install vim-plug (https://github.com/junegunn/vim-plug)

curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

## then run nvim and execute :PlugInstall
```
